<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Sistema Gerenciador de Contatos</title>
		
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
	<link rel="stylesheet" href="css/style.css">
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"></script>

	<script type="text/javascript">
		$(document).ready(function(){
		$('[data-toggle="tooltip"]').tooltip();
		});
	</script>
	
	<?php 
		include_once("request/conexao-banco.php"); 
		/*$consultar_contatos = mysqli_query($conexao_banco, "SELECT * FROM contato INNER JOIN telefone;");
			if($consultar_contatos === FALSE) {
				die(mysqli_error());
			}*/
	?>
</head>
<body>
    <div class="container">
        <div class="table-wrapper">
            <div class="table-title"  style="background-image: linear-gradient(to #0055c9, #0a80ce , #18b9d4);">
                <div class="row">
                    <div class="col-sm-5">
						<h2>Sistema Gerenciador de Contatos<b></b></h2>
					</div>
					<div class="col-sm-7">
						<a href="#" class="btn btn-success" data-toggle="modal" data-target="#formularioModal"><i class="fa fa-plus-circle"></i> <span>Adicionar Contato</span></a>
						<a href="gerar-pdf.php" target="blank" class="btn btn-primary"><i class="material-icons">&#xE24D;</i> <span>Imprimir</span></a>				
						<div class="search-box">
                            <i class="fa fa-search"></i>
                            <input type="text" class="form-control" placeholder="Pesquisar">
                        </div>						
					</div>
                </div>
            </div>
			<table class="table table-striped table-hover">
				<thead>
					<tr style="background-color: #299be4;">
						<th class="title">ID</th>
						<th class="title">Nome</th>						
						<th class="title">Telefone</th>
						<th class="title">E-mail</th>
						<th class="title">Criado em</th>
						<th class="title">Ações</th>
					</tr>
				</thead>
				<?php
					include_once("request/conexao-banco.php");
						$listar_contatos = mysqli_query($conexao_banco, "SELECT * FROM contato INNER JOIN telefone ON contato.id_contato=telefone.contato_id_contato INNER JOIN endereco ON endereco.contato_id_contato=contato.id_contato;");
						$cont=0;
						$qtd=0;
						$diretorio='imagens/fotos/';

					while($rows_contato = mysqli_fetch_assoc($listar_contatos)){
						$cont++;
						$qtd++;
                ?>
				<tbody>
					<tr>
						<td><?php echo $qtd; ?></td>
						<td><a href="#"><img src="imagens/fotos/<?php echo $rows_contato['foto']; ?>" class="avatar" alt="Avatar" width="35px" height="35px"> <?php echo $rows_contato['nome']; ?> <?php echo $rows_contato['sobrenome']; ?></a></td>
						<td>(<?php echo $rows_contato['ddd']; ?>) <?php echo $rows_contato['numero']; ?></td>                        
						<td><?php echo $rows_contato['email']; ?></td>
						<td><?php echo  date('d/m/Y', strtotime($rows_contato['data_criacao'])); ?></td>
						<td>
							<a type="button" href="#" class="settings" title="Editar" data-toggle="modal" data-target="#editarRegistroModal" onclick="carregar_contato('<?php echo $rows_contato['id_contato']; ?>','<?php echo $rows_contato['nome']; ?>','<?php echo $rows_contato['sobrenome']; ?>','<?php echo $rows_contato['email']; ?>','<?php echo $rows_contato['ddd']; ?>','<?php echo $rows_contato['numero']; ?>','<?php echo $rows_contato['rua']; ?>','<?php echo $rows_contato['numero_casa']; ?>','<?php echo $rows_contato['bairro']; ?>','<?php echo $diretorio.$rows_contato['foto']; ?>')"><i class="fa fa-cog"></i></a>
							<a href="#" class="delete" title="Apagar" data-toggle="modal" data-target="#excluirRegistroModal<?php echo $rows_contato['id_contato']; ?>"><i class="fa fa-times-circle"></i></a>
								<?php include "modais.php" ?>
						</td>
					</tr>
                </tbody>
				<?php 	
					}
					if ($cont == 0){
					echo "<p style='font-weight: bold; color: #566787; margin-left: 40%;'>Você não tem nenhum contato para mostrar.</p>";
					}
				?>
            </table>
			<div class="clearfix">
                <div class="hint-text">Mostrando <b><?php echo $qtd; ?></b> de <b><?php echo $qtd; ?></b> registros</div>
                <ul class="pagination">
                    <li class="page-item disabled"><a href="#">Anterior</a></li>
                    <li class="page-item active"><a href="#" class="page-link">1</a></li>
                    <li class="page-item"><a href="#" class="page-link">Próximo</a></li>
                </ul>
            </div>
        </div>
    </div>
	<footer class="footer-social-icon section_padding_50 text-footer">
		<div class="copyright-text text-center">
			<hr>
			<p id="cor-preto-padrao">Desafio FlexPeak © 2020. Desenvolvido por <a href="https://www.linkedin.com/in/maik-elamide" target="_blank" style="text-decoration: underline;" id="cor-preto-padrao">Maik Elamide</a>.</p>
		</div>
    </footer>
	<?php
		include "modais.php"
	?>
	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
	
	<script>
		function carregar_contato(id_contato, nome, sobrenome, email, ddd, telefone, rua, numero_casa, bairro, foto){
			$('#id_contato').val(id_contato);
			$('#id_nome').val(nome);
			$('#id_sobrenome').val(sobrenome);
			$('#id_email').val(email);
			$('#id_ddd').val(ddd);
			$('#id_telefone').val(telefone);
			$('#id_rua').val(rua);
			$('#id_numero_casa').val(numero_casa);
			$('#id_bairro').val(bairro);
			$('#id_foto').attr('src', foto);
		}
	</script>
</body>
</html>                                		                            