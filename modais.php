<!-- Modal -->
	<div class="modal fade" id="formularioModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header" style="background-color: #299be4;">
					<h5 class="modal-title" id="exampleModalCenterTitle" style="color: white;"><i class="fa fa-address-card"></i> Adicionar Contato</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<form method="post" action="request/cadastrar-contato.php" enctype="multipart/form-data">
					<div class="modal-body">
							<div class="form-group input-group">
								<div class="input-group-prepend">
									<span class="input-group-text"> <i class="fa fa-user"></i> </span>
								</div>
								<input name="campoNome" class="form-control" placeholder="Nome" type="text">
								<input name="campoSobrenome" class="form-control" placeholder="Sobrenome" type="text">
							</div>
							<div class="form-group input-group">
								<div class="input-group-prepend">
									<span class="input-group-text"> <i class="fa fa-envelope"></i> </span>
								</div>
								<input name="campoEmail" class="form-control" placeholder="E-mail" type="email">
							</div>
							<div class="form-group input-group">
								<div class="input-group-prepend">
									<span class="input-group-text"> <i class="fa fa-phone"></i> </span>
								</div>
								<div style="width:70px;">
								<input name="campoDDD" class="form-control" placeholder="DDD" type="tel">
								</div>
								<input name="campoTelefone" class="form-control" placeholder="Número do telefone" type="tel">
							</div>
						<div class="form-group input-group">
							<div class="input-group-prepend">
								<span class="input-group-text"> <i class="fa fa-building"></i> </span>
							</div>
							<div style="width:205px;">
								<input name="campoEndereco" class="form-control" placeholder="Endereço" type="text">
							</div>
							<div style="width:70px;">
								<input name="campoNumero" class="form-control" placeholder="nº" type="text">
							</div>
							<div style="width:135px;">
								<input name="campoBairro" class="form-control" placeholder="Bairro" type="text">
							</div>
						</div>
						<!--
						<div class="form-group input-group">
							<div style="width:232px;">
								<select name="id_estado" id="id_estado" class="browser-default custom-select" required>
									<option value="" disabled selected >Selecione o Estado</option>
									<?php 
										include_once("request/conexao-banco.php"); 
										$result = "SELECT nome FROM estado";
										$resultado = mysqli_query($conexao_banco, $result);
										while($row = mysqli_fetch_assoc($resultado)) {
										echo '<option value="'.$row['id_estado'].'"> '.$row['nome'].' </option>';
										}
									?> 
								</select>
							</div>
							<div style="width:232px;">
								<select name="id_cidade" id="id_cidade" class="browser-default custom-select" required>
									<option value="" disabled selected >Selecione a Cidade</option>
								</select>
							</div>
						</div>	
						-->
						<div class="form-group input-group">
							<div class="input-group-prepend">
								<span class="input-group-text"> <i class="fa fa-image"> Selecione foto</i></span>
							</div>
							<input class="form-control" type="file" name="imagem" accept="image/*"/>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
						<input type="reset" class="btn btn-secondary" value="limpar"></input>
						<button type="submit" class="btn btn-success">Salvar</button>
					</div>
				</form>
				<script type="text/javascript" src="https://www.google.com/jsapi"></script>
				<script type="text/javascript">
					google.load("jquery", "1.4.2");
				</script>
				
				<script type="text/javascript">
					$(function(){
						$('#id_estado').change(function(){
							if( $(this).val() ) {
								$('#id_cidade').hide();
								$('.carregando').show();
								$.getJSON('../request/select_cidades.php?search=',{id_estado: $(this).val(), ajax: 'true'}, function(j){
									var options = '<option value="">Escolha a Cidade</option>';	
									for (var i = 0; i < j.length; i++) {
										options += '<option value="' + j[i].id_cidade + '">' + j[i].nome + '</option>';
									}	
									$('#id_cidade').html(options).show();
									$('.carregando').hide();
								});
							} else {
								$('#id_cidade').html('<option value="">– Escolha a Cidade –</option>');
							}
						});
					});
				</script>
				
			</div>
		</div>
	</div>
	<!-- ./modal-->
	<div class="modal fade" id="excluirRegistroModal<?php echo $rows_contato['id_contato']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header" style="background-color: #299be4;">
					<h5 class="modal-title" style="color: white;"><i class="fa fa-exclamation-triangle"></i> Atenção</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<h5>Deseja realmente apagar o registro?</h5>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal">Não</button>
					<a href="request/excluir-contato.php?id=<?php echo $rows_contato['id_contato']; ?>">
						<button type="button" class="btn btn-success">Sim</button>
					</a>
				</div>
			</div>
		</div>
	</div>
	<!-- ./Modal -->
	<div class="modal fade" id="editarRegistroModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header" style="background-color: #299be4;">
					<h4 class="modal-title" id="exampleModalCenterTitle" style="color: white;"><i class="fa fa-edit"></i> Editar Contato</h4>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<form method="POST" action="request/editar-contato.php" enctype="multipart/form-data">
					<div class="modal-body">
						<div class="form-group input-group">
							<div class="input-group-prepend">
								<span class="input-group-text"> <i class="fa fa-user"></i> </span>
							</div>
							<input type="hidden" id="id_contato" name="idContato"/>
							<input name="editNome" id="id_nome" class="form-control" placeholder="Nome" type="text">
							<input name="editSobrenome" id="id_sobrenome"class="form-control" placeholder="Sobrenome" type="text">
						</div>
						<div class="form-group input-group">
							<div class="input-group-prepend">
								<span class="input-group-text"> <i class="fa fa-envelope"></i> </span>
							</div>
							<input name="editEmail" id="id_email" class="form-control" placeholder="E-mail" type="email">
						</div>
						<div class="form-group input-group">
							<div class="input-group-prepend">
								<span class="input-group-text"> <i class="fa fa-phone"></i> </span>
							</div>
							<div style="width:70px;">
								<input name="editDdd" id="id_ddd" class="form-control" placeholder="DDD" type="tel">
							</div>
							<input name="editTelefone" id="id_telefone" class="form-control" placeholder="Número do telefone" type="tel">
						</div>
						<div class="form-group input-group">
							<div class="input-group-prepend">
								<span class="input-group-text"> <i class="fa fa-building"></i> </span>
							</div>
							<div style="width:205px;">
								<input name="editRua" id="id_rua" class="form-control" placeholder="Endereço" type="text">
							</div>
							<div style="width:70px;">
								<input name="editNumero" id="id_numero_casa" class="form-control" placeholder="nº" type="text">
							</div>
							<div style="width:135px;">
								<input name="editBairro" id="id_bairro" class="form-control" placeholder="Bairro" type="text">
							</div>
						</div>
						<div class="my-4">
							<i class="fa fa-image"></i>
							<label for="id_foto">Foto</label>
							<img id="id_foto" width="200px" height="200px" src="" style="border-radius: 100px;"/>	
						</div>
						<div class="form-group input-group">
							<div class="input-group-prepend">
								<span class="input-group-text"> <i class="fa fa-image"></i> Selecione uma imagem para alterar</span>
							</div>
							<div>
								<input name="editFoto" class="form-control" placeholder="Anexar foto" title="Anexar foto" type="file" accept="image/*">
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
						<button type="submit" class="btn btn-success">Alterar</button>
					</div>
				</form>	
			</div>
		</div>
	</div>