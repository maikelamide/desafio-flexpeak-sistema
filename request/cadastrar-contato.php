﻿<?php
    session_start();
    include_once("conexao-banco.php");
	$usuario_logado = $_SESSION['id_usuario_logado'];
	
	$msg = false;
    //Receber os dados do formulário
    $nome = filter_input(INPUT_POST, 'campoNome', FILTER_SANITIZE_STRING);
	$sobrenome = filter_input(INPUT_POST, 'campoSobrenome', FILTER_SANITIZE_STRING);
	$email = filter_input(INPUT_POST, 'campoEmail', FILTER_SANITIZE_STRING);
	$ddd = filter_input(INPUT_POST, 'campoDDD', FILTER_SANITIZE_STRING);
	$telefone = filter_input(INPUT_POST, 'campoTelefone', FILTER_SANITIZE_STRING);
	$endereco = filter_input(INPUT_POST, 'campoEndereco', FILTER_SANITIZE_STRING);
	$numero = filter_input(INPUT_POST, 'campoNumero', FILTER_SANITIZE_STRING);
	$bairro = filter_input(INPUT_POST, 'campoBairro', FILTER_SANITIZE_STRING);
	$nome_imagem = $_FILES['imagem']['name'];   

	if(isset($_FILES['imagem'])){
		$diretorio = "../imagens/fotos/";
		
		//move_uploaded_file($_FILES['pdf']['tmp_name'], $diretorio.$nome_pdf);
		move_uploaded_file($_FILES['imagem']['tmp_name'], $diretorio.$nome_imagem);
	
		//Inserir no BD
		$query_cadastro = mysqli_query($conexao_banco,"INSERT INTO contato(nome, sobrenome, email, foto) VALUES ('$nome', '$sobrenome', '$email','$nome_imagem')");	
		$id = mysqli_insert_id($conexao_banco);
		$query2 = mysqli_query($conexao_banco, "INSERT INTO endereco(contato_id_contato, rua, numero_casa, bairro) VALUES ('$id', '$endereco', '$numero','$bairro')");
		$query3 = mysqli_query($conexao_banco, "INSERT INTO telefone(contato_id_contato, numero, ddd) VALUES ('$id', '$telefone', '$ddd')");
		//Verifica se foi salvo
		if(mysqli_affected_rows($conexao_banco) > 0){  ?>
    <script>
        alert('Cadastro realizado com sucesso!');
        document.location.href = '../index.php';
    </script>
    <?php
    exit;
    }else{  ?>
    <script>
        alert('Falha ao cadastrar contato!');
        document.location.href = '../index.php';
    </script>
    <?php
    exit;
    }
    $conexao_banco->close();
	}
?>